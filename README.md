# - What is this script and what does it do?
# - This script is exactly what it's named, "Softether-AIO" (All In One)
# - It can completely setup SoftEther without the user (you) having any knowledge on how to, all you have to do is input the needed information when it's asked
# -| This script is a SoftEther AIO, it can execute the majority of options that Softether offers, Those options fall into the following catagory's.
# - $= [[ SoftEther v4.27-9968 ]]
# - {Not including the Auto Setup option in the list below but it is an option}
# - 1.) User & Group
# - -%1-  Allows you to Create Users, delete Users, Add Expiration date for user, List Users, List Policies, Set User Policy, Delete User Policy, Change User Information [All the same for the groups]
# - 2.) Port Listening
# - -%2-  Allows you to Create New TCP Listener, Delete TCP Listener, Stop TCP Listener Operation, Begin TCP Listener Operation, List TCP Listeners
# - 3.) Logging
# - -%3-  Disable Security Log &(or) Packet Log, Enable Security Log &(or) Packet Log, Get List of Log Files, Get Log Save Setting of Virtual Hub
# - 4.) Session
# - -%4-  Get List of Connected Sessions, Get Session Information, Disconnect Session
# - 5.) Connection
# - -%5-  List TCP Connections Connecting to the VPN Server, Get Info of TCP Connections Connecting to the VPN Server, Disconnect TCP Connections Connecting to the VPN Server
# - 6.) DHCP
# - -%6-  Enable Virtual DHCP Server Function of SecureNAT Function, Get Virtual DHCP Server Function Setting of SecureNAT Function, Change Virtual DHCP Server Function Setting of SecureNAT Function, Get Virtual DHCP Server Function Lease Table of SecureNAT Function
# - 7.) Hub 
# - -%7-  Create New Virtual Hub, Create New Dynamic Virtual Hub (For Clustering), Create New Static Virtual Hub (For Clustering), Delete Virtual Hub, Get List of Virtual Hubs, Change Virtual Hub Type to Dynamic Virtual Hub, Change Virtual Hub Type to Static Virtual Hub
# - 8.) NAT
# - -%8-  Disable Virtual NAT Function of SecureNAT Function,Enable Virtual NAT Function of SecureNAT Function, Get Virtual NAT Function Setting of SecureNAT Function, Change Virtual NAT Function Setting of SecureNAT Function, Get Virtual NAT Function Session Table of SecureNAT Function
# - 9.) Admin
# - -%9-  Get List of Virtual Hub Administration Options, Set Values of Virtual Hub Administration Options
# - =$