# - What is this script and what does it do?
# - It can completely setup SoftEther without the user (you) having any knowledge on how to, all you have to do is input the needed information when it's asked
# -| This script is a SoftEther AIO, it can execute the majority of options that Softether offers, Those options fall into the following catagory's.
# - $= [[ SoftEther v4.27-9968 ]]
# - {Not including the Auto Setup option in the list below but it is an option}
# - 1.) User & Group
# - -%1-  Allows you to Create Users, delete Users, Add Expiration date for user, List Users, List Policies, Set User Policy, Delete User Policy, Change User Information [All the same for the groups]
# - 2.) Port Listening
# - -%2-  Allows you to Create New TCP Listener, Delete TCP Listener, Stop TCP Listener Operation, Begin TCP Listener Operation, List TCP Listeners
# - 3.) Logging
# - -%3-  Disable Security Log &(or) Packet Log, Enable Security Log &(or) Packet Log, Get List of Log Files, Get Log Save Setting of Virtual Hub
# - 4.) Session
# - -%4-  Get List of Connected Sessions, Get Session Information, Disconnect Session
# - 5.) Connection
# - -%5-  List TCP Connections Connecting to the VPN Server, Get Info of TCP Connections Connecting to the VPN Server, Disconnect TCP Connections Connecting to the VPN Server
# - 6.) DHCP
# - -%6-  Enable Virtual DHCP Server Function of SecureNAT Function, Get Virtual DHCP Server Function Setting of SecureNAT Function, Change Virtual DHCP Server Function Setting of SecureNAT Function, Get Virtual DHCP Server Function Lease Table of SecureNAT Function
# - 7.) Hub 
# - -%7-  Create New Virtual Hub, Create New Dynamic Virtual Hub (For Clustering), Create New Static Virtual Hub (For Clustering), Delete Virtual Hub, Get List of Virtual Hubs, Change Virtual Hub Type to Dynamic Virtual Hub, Change Virtual Hub Type to Static Virtual Hub
# - 8.) NAT
# - -%8-  Disable Virtual NAT Function of SecureNAT Function,Enable Virtual NAT Function of SecureNAT Function, Get Virtual NAT Function Setting of SecureNAT Function, Change Virtual NAT Function Setting of SecureNAT Function, Get Virtual NAT Function Session Table of SecureNAT Function
# - 9.) Admin
# - -%9-  Get List of Virtual Hub Administration Options, Set Values of Virtual Hub Administration Options
# - =$
import subprocess
def hakka(cmd):
 subprocess.call(cmd, shell=True)
hakka('clear')
print "SoftEther menu made by Tanner @Assert.lmao\nWith help from Venom @yourv3nom on instagram"
hakka("sleep 2")
def start():
 print """
1.) Setup SoftEther
2.) User & Group Options 
3.) Port Listening Options
4.) Logging Options
5.) Session Options
6.) Connection Options
7.) DHCP Options
8.) Hub Options
9.) NAT Options
0.) Admin Options
\n Type "bug" if you find something that doesn't work
  """
 menu = raw_input("What option would you like to do? ---> ")
 if menu == "1":
    server = "wget http://www.softether-download.com/files/softether/v4.27-9668-beta-2018.05.29-tree/Linux/SoftEther_VPN_Server/64bit_-_Intel_x64_or_AMD64/softether-vpnserver-v4.27-9668-beta-2018.05.29-linux-x64-64bit.tar.gz"
    client = "wget http://www.softether-download.com/files/softether/v4.27-9668-beta-2018.05.29-tree/Linux/SoftEther_VPN_Client/64bit_-_Intel_x64_or_AMD64/softether-vpnclient-v4.27-9668-beta-2018.05.29-linux-x64-64bit.tar.gz"
    script = "curl https://pastebin.com/raw/6SdqXQFc > /etc/init.d/vpnserver/vpnserver"
    make_s = "curl https://pastebin.com/raw/B26X7vg7 > server.sh"
    make_c = "curl https://pastebin.com/raw/Yii9pbzu > client.sh"
    service = "curl https://pastebin.com/raw/h3fQBNVQ > service.sh"
    setup = "curl https://pastebin.com/raw/9FMUQjDg > setup.py"
    mv = "curl https://pastebin.com/raw/MpdWTPrv > move.sh"
    hakka('clear')
    print "SoftEther Setup by Tanner!" 
    hakka("sleep 5")
    OS = raw_input("Debian or CentOS? ---> ") 
    hakka("echo Downloading needed dependancies...") 
    hakka("sleep 5")
    if OS == "Debian":
     hakka("apt-get update && apt-get upgrade -y;apt-get install build-essential -y")
    elif OS == "CentOS": 
     hakka('yum groupinstall "Development Tools" -y; yum install nano -y; yum install wget -y')
    hakka("clear")
    hakka("echo Downloading server side files...") 
    hakka("sleep 5")
    hakka(server)
    hakka("clear")  
    hakka("echo Extracting files...") 
    hakka("sleep 5")
    hakka("tar xzvf softether-vpnserver*") 
    hakka("clear")  
    hakka("echo Making .sh file then compiling server...") 
    hakka("sleep 5")
    hakka(make_s) 
    hakka("chmod 777 server.sh") 
    hakka("./server.sh") 
    hakka("clear")
    hakka("echo Making startup script to start the vpnserver on boot...") 
    hakka("sleep 5")
    hakka(script)
    if OS == "Debian":
     hakka("chkconfig --add vpnserver | update-rc.d vpnserver defaults")  
    elif OS == "CentOS": 
     hakka("chmod 755 /etc/init.d/vpnserver && /etc/init.d/vpnserver start") 
    hakka("clear")  
    hakka("echo Downloading client files...") 
    hakka("sleep 5")
    hakka(client)
    hakka("clear")  
    hakka("echo Unziping the file...")
    hakka("sleep 5")
    hakka("tar xzvf softether-vpnclient*") 
    hakka("clear")  
    hakka("echo Making shell file that will compile the server-side files...")
    hakka("sleep 5")
    hakka(make_c)
    hakka("chmod 777 *")
    hakka("sh client.sh")
    hakka("sleep 5") 
    hakka('echo Moving "vpnserver" and "vpnclient" to /usr/local/...') 
    hakka("sleep 5")
    hakka(mv)
    hakka("sh move.sh")
    hakka("clear")   
    print "Starting VPN server and client serices..."
    hakka(service)
    hakka("sh service.sh")
    hakka("sleep 5")
    hakka("clear")
    hakka("echo Put in the needed information in a few seconds...")
    hakka("sleep 5")
    hakka(setup)
    hakka("python setup.py")
    hakka("echo Deleting uneeded files...")
    hakka("sleep 3")
    hakka("rm -rf *.gz")
    hakka("rm -rf server.sh")
    hakka("rm -rf client.sh")
    print """Okay everything is done lmao gg mane we rekt those scrubs lol
    -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    This script was made by Tanner! Pm me if you need, here is my contact info:
    @Assert.lmao on Instagram
    email: Tanner@Atheist.com
    """ ## CONTACT
    hakka("rm -rf *.sh")
    hakka("rm -rf setup.py")
    hakka("rm -rf mv.sh")
    hakka("rm -rf S*.py")
 if menu == "2":
    ## THANKS VENOM FOR THE 100 LINES OF HELP DADDYO ILYSM
  def start_1():
   hakka('clear')
   print """
  1.) User Options
  2.) Group Options"""
   options_menu = raw_input("Which Options would you like to access? --> ")
   if "1" in options_menu:
    hakka("clear")   
    print """
Here are the available options to choose from:
1.) Create User
2.) Delete User
3.) Set User's Expiration Date
4.) Get User Information
5.) List of Users
6.) Policy List
7.) Set User Security Policy
8.) Delete User Security Policy
9.) Change User Information
    """
    user = raw_input("Which Option would you like to choose from? --> ")
    if "1" in user:
      	hakka("clear")
        a_user = raw_input("What do you want the username to be? --> ")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:UserCreate "+ a_user +" /GROUP:none /REALNAME:none /NOTE:none")
        password = raw_input("What would you like the password for this user to be? --> ")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:UserPasswordSet "+ a_user +" /PASSWORD:"+ password +"")
        print "User %s/ has been added!" % a_user
        hakka("sleep 4")
        hakka("clear")
        start_1()
    if "2" in user:
      	hakka("clear")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:SessionList")
        d_user = raw_input("What user would you like to delete? --> ")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:Userdelete "+ d_user +"")
        print "User %s has been deleted!" % d_user
        hakka("sleep 4")
        hakka("clear")
        start_1()
    if "3" in user:
      	hakka("clear")
        name = raw_input("What is the username of the user you wish to set an expiration date for? --> ")
        expire = raw_input("When would you like the account to expire? --> (example format: 2005/10/08 19:30:00)")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:userexpiresset "+ name +" /EXPIRES:"+ expire +"")
        print "Expiration date has been added to the user %s" % name
        hakka("sleep 4")
        hakka("clear")
        start_1()
    if "4" in user:
      	hakka("clear")
        u_get = raw_input("What is the name of the user? --> ")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:UserGet "+ u_get +"")
        print "Here is the information for the user %s" % u_get
        sdsdsa = raw_input("Press ENTER key to return to main menu when finished looking!")
        hakka("clear")
        start_1()
    if "5" in user: 
      	hakka("clear")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:userlist")
        print "Displaying list of all users"
        ksjhd = raw_input("Press ENTER key to return to main menu when finished looking!")
        hakka("clear")
        start_1()
    if "6" in user: 
      	hakka("clear")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:policylist")
        print "Displaying all of the policy options!"
        raw_input("Press ENTER key to return to main meny when finished looking!")
        hakka("clear")
        start_1()
    if "7" in user: 
      	hakka("clear")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:policylist")
        us_policy = raw_input("What user are we setting a policy for? --> ")
        p_name = raw_input("Which policy are we setting? --> ")
        p_value = ("Whats the policy value? --> ")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:userpolicyset "+ us_policy +" /NAME:"+ p_name +" /VALUE:"+ p_value +"")
        print "Policy has been set for user %s!" % us_policy
        hakka("sleep 4")
        hakka("clear")
        start_1()
    if "8" in user:
      	hakka("clear")
        ur_policy = raw_input("What is the name of the user you wish to remove policies for? --> ")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:userpolicyremove "+ ur_policy +"")
        print "Policy has been removed for user %s!" % ur_policy
        hakka("clear")
        start_1()
    if "9" in user: 
      	hakka("clear")
        u_set = raw_input("Whats the user who's inforamtion you would like to change? --> ")
        u_group = raw_input("Whats the group name? (if no group name type 'none'")
        u_realname = raw_input("Whats the users real name? --> ?")
        u_note = raw_input("Any notes about the user? --> ")
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:userset "+ u_set +" /GROUP:"+ u_group +" /REALNAME:"+ u_realname +" /NOTE:"+ u_note +"")
        print "Changed information for the user %s!" % u_set
        hakka("sleep 4")
        hakka("clear")
        start_1() 
   if "2" in options_menu:
    hakka("clear") 
    print """
Here are the available options to choose from:
1.) Create Group
2.) Delete Group
3.) Get Group Information and List of Assigned Users
4.) Add User to Group
5.) Get List of Groups
6.) Set Group Security Policy (List them as well)
7.) Delete Group Security Policy
8.) Set Group Information
9.) Delete User from Group
   """
   group = raw_input("What option would you like to choose? --> ")
   if "1" in group:
      hakka("clear")
      c_group = raw_input("What do you want the group name to be? --> ")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:GroupCreate "+ c_group +" /REALNAME:none /NOTE:none")
      print "Group Created!"
      hakka("sleep 4")
      hakka("clear")
      start_1()
   if "2" in group:
      hakka("clear")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:GroupList")
      d_group = raw_input("What group would you like to delete? --> ")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:GroupDelete "+ d_group +"")
      print "Deleted group %s successfully!" % d_group
      hakka("sleep 4")
      hakka("clear")
      start_1()
   if "3" in group:
      hakka("clear")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:GroupList")
      gg_group = raw_input("What group information would you like to get? --> ")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:GroupGet "+ gg_group +"")
      print "successfully got group information!"
      hakka("sleep 4")
      hakka("clear")
      start_1()
   if "4" in group:
      hakka("clear")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:GroupList")
      gj_group = raw_input("What group would you like to add a user to? --> ")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:UserList")
      jg_group = raw_input("What user would you like to add? --> ")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:"+ au_group +" /USERNAME:"+ jg_group +"")
      print "Successfully added user to group!"
      hakka("sleep 4")
      hakka("clear")
      start_1()
   if "5" in group:
      hakka("clear")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:GroupList")
      print "Successfully listed groups!"
      hakka("sleep 4")
      hakka("clear")
      start_1()
   if "6" in group:
      hakka("clear")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:PolicyList")
      raw_input("Press ENTER key to continue...")
      nameof_policy = raw_input("What is the name of the policy you want to set? --> ")
      nameof_group = raw_input("What is the name of the group you want to set a policy on? --> ")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:GroupPolicySet "+ nameof_group +" /NAME:"+ nameof_policy +" /VALUE:True")
      print "Successfully made security policy!"
      hakka("sleep 4")
      hakka("clear")
      start_1()
   if "7" in group:
      hakka("clear")
      rm_group = raw_input("What is the name of the group you want to remove security policies on? --> ")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:GroupPolicyRemove "+ rm_group +"")
      print "Successfully deleted group!"
      hakka("sleep 4")
      hakka("clear")
      start_1()
   if "8" in group:
      hakka("clear")
      set_group = raw_input("What is the name of the group you want to change the information to? --> ")
      est_group = raw_input("What do you want the 'REALNAME' of the group to be? --> ")
      tes_group = raw_input("What note do you want to put on the group? --> ")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:GroupSet "+ set_group +"/REALNAME:"+ est_group +" /NOTE:"+ tws_group +"")
      print "Successfully changed group information!"
      hakka("sleep 4")
      hakka("clear")
      start_1()
   if "9" in group:
      hakka("clear")
      delusrgp_group = raw_input("What is the name of the group you want to delete a user from? --> ")
      delusr_group = raw_input("What user would you like to delete? --> ")
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:GroupUnjoin "+ delusrgp_group +" /NAME:"+ delusr_group +"")
      print "Successfully deleted user from group"
      hakka("sleep 4")
      hakka("clear")
      start_1()
  start_1()
 if menu == "3":
   def start_2():
    hakka("clear") 
    print """
1.) Create New TCP Listener         
2.) Delete TCP Listener
3.) Stop TCP Listener Operation
4.) Begin TCP Listener Operation
5.) Get List of TCP Listeners
    """
   start_2()
   mm = raw_input("What option do you want? -> ")
   if mm == "1":
    p = raw_input("What port would you like to create a TCP Listener on? -> ")
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443  /cmd:ListenerCreate "+ p +"")
    start_2()
    mm = raw_input("What option do you want? -> ")
   if mm == "2":
    pp = raw_input("What TCP Listner do you want to delete? -> ")
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443  /cmd:ListenerDelete "+ pp +"")
    start_2()
    mm = raw_input("What option do you want? -> ")
   if mm == "3":
    ppp = raw_input("What TCP Listner do you want to disable? -> ")
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443  /cmd:ListenerDisable "+ ppp +"")
    start_2()
    mm = raw_input("What option do you want? -> ")
   if mm == "4":
    ppp = raw_input("What TCP Listner do you want to Enable? -> ")
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443  /cmd:ListenerEnable "+ pppp +"")
    start_2()
    mm = raw_input("What option do you want? -> ")
   if mm == "5":
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443  /cmd:ListenerList")
    start_2()
    mm = raw_input("What option do you want? -> ")
   else:
    print "Select one of the options listed!"
    start_2()
    mm = raw_input("What option do you want? -> ")
 if menu == "4":
   def start_3():
    hakka("clear") 
    print """
1.) Disable Security Log or Packet Log
2.) Enable Security Log or Packet Log
3.) Get List of Log Files
4.) Get Log Save Setting of Virtual Hub
    """
    log = raw_input("What option would you like to select? --> ")
    if "1" in log:
     dis = raw_input("Would you like to disable Packet logs or Security logs? --> ")
     hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:LogDisable "+ dis +"")
     print "Successfully disabled %s Logs!" % dis
     hakka("sleep 4")
     hakka("clear")
     start_3()
    if "2" in log:
     en = raw_input("Would you like to enable Packet logs or Security logs? --> ")
     hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:LogEnable "+ en +"")
     print "Successfully enabled %s Logs!" % en
     hakka("sleep 4")
     hakka("clear")
     start_3()
    if "3" in log:
     hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:Logfillist")
     print "Successfully listed logs!"
     start_3()
    if "4" in log:
     hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:Logfileget")
     print "Successfully got file!"
     start_3()
   start_3()
 if menu == "5":
   def start_4():
    hakka("clear") 
    print """Here are the available options to choose from:
1.) Get List of Connected Sessions
2.) Get Session Information
3.) Disconnect Session"""
    answer_ses = raw_input("Which option would you like to choose? --> ")
    if "1" in answer_ses:
      hakka("clear")
      print "Here are the current user sessions:"
      hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:SessionList")
      raw_input("Press ENTER to return to menu...")
      hakka("clear")
      start_4()
      answer = raw_input("Which option would you like to choose? --> ")
    if "2" in answer_ses:
        SID = raw_input("What is the SID of the Session? Just copy the session ID (SID) --> ")
        hakka("clear")
        print "Here is the current session information"
        hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:SessionGet "+ SID +"")
        raw_input("Press ENTER to return to menu...")
        hakka("clear")
        start_4()
    if "3" in answer_ses:
     SID1 = raw_input("Which session would you like to disconnect? Just copy the session ID (SID) --> ")
     hakka("clear")
     print "Session has been disconnected!"
     hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:SessionDisconnect "+ SID1 +"")
     raw_input("Press ENTER to return to menu...")
     hakka("clear")
     start_4()
   start_4()
 if menu == "6":
  def main_6():
   hakka("clear") 
   print """
1.) Get List of TCP Connections Connecting to the VPN Server
2.) Get Information of TCP Connections Connecting to the VPN Server
3.) Disconnect TCP Connections Connecting to the VPN Server
   """
   option_conn = raw_input("What option would you like to execute? -> ")
   if "1" in option_conn:
     hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /cmd:ConnectionList")
     print "Successfully listed connections!"
     main_6()
   if "2" in option_conn:
     num2 = raw_input("What are the numbers at the end of the 'CID-'? -> ")
     hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /cmd:ConnectionGet CID-"+ num2 +"")
     print "Successfully got CID-%s's information!" % num2
     main_6()
   if "3" in option_conn:
     num = raw_input("What are the numbers at the end of the 'CID-'? -> ")
     hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /cmd:ConnectionDisconnect CID-"+ num +"")
     print "successfully Disconnected CID-%s!" % num
     main_6()
  main_6()
 if menu == "7":
  def main_7():
   hakka("clear") 
   print """
1.) Enable Virtual DHCP Server Function of SecureNAT Function
2.) Get Virtual DHCP Server Function Setting of SecureNAT Function
3.) Change Virtual DHCP Server Function Setting of SecureNAT Function
4.) Get Virtual DHCP Server Function Lease Table of SecureNAT Function
   """
   option_dhcp = raw_input("What option would you like to execute? -> ")
   if "1" in option_dhcp:
     hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /Hub:VPN /cmd:DHCPEnable")
     print "Successfully Enabled DHCP!"
     main_7()
   if "2" in option_dhcp:
     hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /Hub:VPN /cmd:DHCPGet")
     print "Successfully got the DHCP Settings!"
     main_7()
   if "3" in option_dhcp:
     start_dhcp = raw_input("What is the start of the ip range you want? (internel you nut, example 10.0.0.1) ---> ")
     end_dhcp = raw_input("What is the end of the ip range you want? (internel you nut, example 10.0.0.255) ---> ")
     mask_dhcp = raw_input("What subnet mask would you like your clients to have? (example 255.255.0.0) ---> ")
     expire_dhcp = raw_input("Specify the expiration date in second units for leasing an IP address to a client <-- do that ---> ")
     gw_dhcp = raw_input("What do you want your gateway ip to be? (example: 10.0.0.1)")
     dns_dhcp = raw_input("What do you want your primary DNS server to be? (example: 8.8.8.8, that's google's primary DNS dude) ---> ")
     dns2_dhcp = raw_input("What do you want your primary DNS server to be? (example: 8.8.4.4, that's google's secondary DNS dude) ---> ")
     domain_dhcp = raw_input("Specify the domain name to be notified to the client (or just press ENTER) ---> ")
     log_dhcp = raw_input("Do you wanna enable security logs? (yes or no) ---> ")
     push_dhcp = raw_input("""
Specify the static routing table to push.
Example: "192.168.5.0/255.255.255.0/192.168.4.254, 10.0.0.0/ 255.0.0.0/192.168.4.253
Split multiple entries (maximum: 64 entries) by comma or space
characters. Each entry must be specified in the "IP network
address/subnet mask/gateway IP address" format.\n enter whatever your suppose to ---> """)
     hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /Hub:VPN /cmd:DHCPSet /START:"+ start_dhcp +" /END:"+ end_dhcp +" /MASK:"+ mask_dhcp +" /EXPIRE:"+ expire_dhcp +" /GW:"+ gw_dhcp +" /DNS:"+ dns_dhcp +" /DNS2:"+ dns2_dhcp +" /DOMAIN:"+ domain_dhcp +" /LOG:"+ log_dhcp +" /PUSHROUTE:"+ push_dhcp +"") ##SO MUCH OF A PAIN WHY WOULD YOU DO THIS OIERHNESOINRG;SOIERNGEDFOGSFND;GOIENG;ESITNGSIT;NSR;OIGNRS;GOISNDFGIS;URTNGS;RIOTNGSRNT;IONSRTGI;OHNSRT;GIHONSOITNSDT;OIHNSFG;KJBFSN JI;KRSNG;OSIURTNGSZOTID;GNSRTI;O SRTIOG;SNRTOIGSJ RNVT[ IOGJSROIRTJGIOSRTGHNOSIRTN S;OTIBNRSTKLHBJNTGLKJDFGNS;ILTUKJNH45WIEUT5H4WRYIGSURTKJB TIKUB5I8TDRH5EOUTNHGETIUGNEROIGNE509RTIJ5RTIU9YHRN59Y8IWH5TRNGUOT5N0RSIG8NAER[0G8IHNRSTILGUOSNTRGIUES5NRGT9IH5ENST9RGUHRN5STHIOSRTNG8IORSTNGHPIR6TUGNHPRI6TFGUKJHNRTIUFGCKNHTRF
     main_7()
   if "4" in option_dhcp:
     hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /Hub:VPN /cmd:DHCPTable")
     print "successfully got the table!"
     main_7()
  main_7()
 if menu == "8":
  def main_8():
   hakka("clear") 
   print """
1.) Create New Virtual Hub
2.) Create New Dynamic Virtual Hub (For Clustering)
3.) Create New Static Virtual Hub (For Clustering)
4.) Delete Virtual Hub
5.) Get List of Virtual Hubs
6.) Change Virtual Hub Type to Dynamic Virtual Hub
7.) Change Virtual Hub Type to Static Virtual Hub
   """
   Hub_script = raw_input("What option would you like to do? ---> ")
   if "1" in Hub_script:
    create = raw_input("What do you want the hub to be named? ---> ")
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /cmd:hubcreate "+ create +" /PASSWORD:")
    print "Successfully created hub!"
    main_8()
   if "2" in Hub_script:
    create_1 = raw_input("What do you want the hub to be named? ---> ")
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /cmd:hubcreatedyn "+ create_1 +" /PASSWORD:")
    print "Successfully created hub!"
    main_8()
   if "3" in Hub_script:
    create_2 = raw_input("What do you want the hub to be named? ---> ")
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /cmd:hubcreatesta "+ create_2 +" /PASSWORD:")
    print "Successfully created hub!"
   if "4" in Hub_script:
    deel = raw_input("What is the name of the hub you want to delete? ---> ")
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443  /cmd:hubdel "+ deel +"")
    print "Successfully deleted hub!"
    main_8()
   if "5" in Hub_script:
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /cmd:hublist")
    print "Successfully listed existing hub's!"
    main_8()
   if "6" in Hub_script:
    name_1 = raw_input("What is the name of the hub? ---> ")
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /Hub:VPN /cmd:hubsetd "+ name_1 +"")
    print "lol I hope you know what you're doing because I don't."
    main_8()
   if "7" in Hub_script:
    name_2 = raw_input("What is the name of the hub? ---> ")
    hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /Hub:VPN /cmd:hubsetst "+ name_2 +"")
    print "lol I hope you know what you're doing because I don't."
    main_8()
  main_8()
 if menu == "9":
  def main_9():
   hakka("clear")
   print """
1.) Disable Virtual NAT Function of SecureNAT Function
2.) Enable Virtual NAT Function of SecureNAT Function
3.) Get Virtual NAT Function Setting of SecureNAT Function
4.) Change Virtual NAT Function Setting of SecureNAT Function
5.) Get Virtual NAT Function Session Table of SecureNAT Function"""
   nat = raw_input("\nWhich Option Would You Like to Choose? --> ") 
   if nat == "1":
    hakka("clear")
    hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:NatDisable")
    print "Nat Has Been Disabled.."
    hakka("sleep 3")
    hakka("clear")
    main_9()
   if nat == "2":
    hakka("clear")
    hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:NatEnable")
    print "Nat Has Been Enabled.."
    hakka("sleep 3")
    hakka("clear")
    main_9()
   if nat == "3":
    hakka("clear")
    hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:NatGet")
    print "Nat information listed above."
    raw_input("Press ENTER key to continue")
    hakka("clear")
    main_9()
   if nat == "4":
    hakka("clear")
    MTU = raw_input("What would you like to set the MTU to? --> ")
    TCP_T = raw_input("How many seconds for the TCP TimeOut? --> ")
    UDP_T = raw_input("How many seconds for the UDP TimeOut? --> ")
    LOG_N =raw_input("Do you want to log the NAT operation to the security log? --> ")
    hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:NatSet /MTU:"+ MTU +" /TCPTIMEOUT:"+ TCP_T +" /UDPTIMEOUT:"+ UDP_T +" /LOG:"+ LOG_N +"")
    print "Nat Has Been Set.."
    hakka("sleep 3")
    hakka("clear")
    main_9()
   if nat == "5":
    hakka("clear")
    hakka("/usr/local/vpnclient/vpncmd /Server localhost:443 /Hub:VPN /cmd:NatTable")
    print "Nat Table Listed Above."
    raw_input("Press ENTER key to continue")
    hakka("clear")
    main_9() 
  main_9()
 if menu == '0':
  def main_10():
    hakka("clear") 
    print """
1.) Get List of Virtual Hub Administration Options
2.) Set Values of Virtual Hub Administration Options"""
    admino = raw_input("Would you like to list options or set one? ---> ")
    if admino == "1":
      hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /Hub:VPN /cmd:adminoptionlist")
      raw_input("Press ENTER when you're done looking at the options...")
      print "Successfully listed admin option!"
      main_10()
    if admino == "2":
      admino_option = raw_input("What is the name of the option you want to set? ---> ")
      admino_val = raw_input("What do you want to set the value of %s to? ---> " % admino_option)
      hakka("/usr/local/vpnserver/vpncmd /Server localhost:443 /Hub:VPN /cmd:adminoptionlist "+ admino_option +" /VALUE:"+ admino_val +"")
      print "Hope you did the correct thing dude lmao"
      main_10()
  main_10()
 if 'Bug' or 'bug' in main():
     hakka('clear')
     print '''Please send me an email or pm on instagram\nhere's my contact information:
Instagram: instagram.com/Assert.lmao
Email: Tanner@atheist.com (or) Tanner@tfwno.gf
\nThanks in advanced, I really apriciate it :)
     '''
     raw_input("Press ENTER to be returned to the main menu...")
     hakka('clear')
     start()
start()

